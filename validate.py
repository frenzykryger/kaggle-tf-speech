# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
r"""Runs a trained audio graph against a WAVE file and reports the results.

The model, labels and .wav file specified in the arguments will be loaded, and
then the predictions from running the model against the audio data will be
printed to the console. This is a useful script for sanity checking trained
models, and as an example of how to use an audio model from Python.

Here's an example of running it:

python tensorflow/examples/speech_commands/label_wav.py \
--graph=/tmp/my_frozen_graph.pb \
--labels=/tmp/speech_commands_train/conv_labels.txt \
--wav=/tmp/speech_dataset/left/a5d485dc_nohash_0.wav

"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tqdm import tqdm

import argparse
import sys

import input_data
import models

import tensorflow as tf
import os

import numpy as np

# pylint: disable=unused-import
from collections import Counter
from tensorflow.python.framework import graph_util
from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio

# pylint: enable=unused-import
import settings
import json

FLAGS = None

def create_inference_graph(wanted_words, sample_rate, clip_duration_ms,
                           clip_stride_ms, window_size_ms, window_stride_ms,
                           dct_coefficient_count, model_architecture):
  """Creates an audio model with the nodes needed for inference.

  Uses the supplied arguments to create a model, and inserts the input and
  output nodes that are needed to use the graph for inference.

  Args:
    wanted_words: Comma-separated list of the words we're trying to recognize.
    sample_rate: How many samples per second are in the input audio files.
    clip_duration_ms: How many samples to analyze for the audio pattern.
    clip_stride_ms: How often to run recognition. Useful for models with cache.
    window_size_ms: Time slice duration to estimate frequencies from.
    window_stride_ms: How far apart time slices should be.
    dct_coefficient_count: Number of frequency bands to analyze.
    model_architecture: Name of the kind of model to generate.
  """

  words_list = input_data.prepare_words_list(wanted_words.split(','))
  model_settings = models.prepare_model_settings(
      len(words_list), sample_rate, clip_duration_ms, window_size_ms,
      window_stride_ms, dct_coefficient_count)
  runtime_settings = {'clip_stride_ms': clip_stride_ms}

  fingerprint_input = tf.placeholder(tf.float32, [None, model_settings['fingerprint_size']], name='fingerprint_input')
  fingerprint_frequency_size = model_settings['dct_coefficient_count']
  fingerprint_time_size = model_settings['spectrogram_length']
  reshaped_input = tf.reshape(fingerprint_input, [
      -1, fingerprint_time_size * fingerprint_frequency_size
  ])

  logits = models.create_model(
      reshaped_input, model_settings, model_architecture, is_training=False,
      runtime_settings=runtime_settings)

  # Create an output to use for inference.
  tf.nn.softmax(logits, name='labels_softmax')


def load_labels(filename):
    """Read in labels, one label per line."""
    return [line.rstrip() for line in tf.gfile.GFile(filename)]


def run_graph(sess, wav_file, labels, input_layer_name, output_layer_name):
    # Feed the audio data as input to the graph.
    #   predictions  will contain a two-dimensional array, where one
    #   dimension represents the input image count, and the other has
    #   predictions per class
    softmax_tensor = sess.graph.get_tensor_by_name(output_layer_name)
    predictions, = sess.run(softmax_tensor, {input_layer_name: [wav_file]})

    # Sort to show labels in order of confidence
    node_id = predictions.argsort()[-1]
    human_string = labels[node_id]
    score = predictions[node_id]
    return human_string, score

def label_wav(train_dir, labels, input_name, output_name):
    """Loads the model and labels, and runs the inference to print predictions."""
    if not train_dir or not tf.gfile.Exists(train_dir):
        tf.logging.fatal('Audio file does not exist %s', train_dir)

    if not tf.gfile.IsDirectory(train_dir):
        tf.logging.fatal('Test directory %s is not directory', train_dir)

    if not labels or not tf.gfile.Exists(labels):
        tf.logging.fatal('Labels file does not exist %s', labels)

    labels_list = load_labels(labels)
    errors = Counter()
    accuracies = {}
    for label in labels_list:
        tf.logging.info("Checking %s", label)

        model_settings = models.prepare_model_settings(
            len(labels_list),
            FLAGS.sample_rate, FLAGS.clip_duration_ms, FLAGS.window_size_ms,
            FLAGS.window_stride_ms, FLAGS.dct_coefficient_count)
        silence_percentage = 200.0 if label == input_data.SILENCE_LABEL else 0
        unknown_percentage = 200.0 if label == input_data.UNKNOWN_WORD_LABEL else 0

        processor_labels = list(set(labels_list) - {input_data.SILENCE_LABEL, input_data.UNKNOWN_WORD_LABEL})
        audio_processor = input_data.AudioProcessor(
            '', train_dir, silence_percentage,
            unknown_percentage,
            processor_labels, FLAGS.validation_percentage,
            FLAGS.testing_percentage, model_settings)

        time_shift_samples = int((FLAGS.time_shift_ms * FLAGS.sample_rate) / 1000)

        valid_fingerprints, valid_ground_truth = audio_processor.get_data(
            FLAGS.samples_per_class, 0, model_settings, FLAGS.background_frequency,
            FLAGS.background_volume, time_shift_samples, 'testing', sess, generate=label)

        assert np.unique(valid_ground_truth).size == 1, str(Counter(valid_ground_truth))

        n_correct = 0
        n_total = 0
        for wav in tqdm(valid_fingerprints, 'checking samples'):
            prediction, score = run_graph(sess, wav, labels_list, input_name, output_name)
            n_total += 1
            if prediction == label:
                n_correct += 1
            else:
                errors[label + ' ' + prediction] += 1
        accuracies[label] = n_correct / n_total
        tf.logging.info("%s accuracy is %f", label, n_correct / n_total)

    tf.logging.info("Error counts: %s", errors.most_common())
    tf.logging.info("Accuracies: %s", accuracies)
    with open('validation_result.json', 'w') as f:
        json.dump([accuracies, errors], f)


def main(_):
    """Entry point for script, converts flags to arguments."""
    tf.logging.set_verbosity(tf.logging.INFO)
    label_wav(FLAGS.train_dir, FLAGS.labels, FLAGS.input_name,
              FLAGS.output_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--train_dir', type=str, default=settings.train_dir, help='Audio files to be identified.')
    parser.add_argument(
        '--start_checkpoint', type=str, help='Model to use for identification.')
    parser.add_argument(
        '--model_architecture',
        type=str,
        default='conv',
        help='What model architecture to use')
    parser.add_argument(
        '--wanted_words',
        type=str,
        default=settings.wanted_words,
        help='Words to use (others will be added to an unknown label)', )
    parser.add_argument(
        '--labels', type=str, default=settings.labels_file,
        help='Path to file containing labels.')
    parser.add_argument(
        '--input_name',
        type=str,
        default='fingerprint_input:0',
        help='Name of WAVE data input node in model.')
    parser.add_argument(
        '--output_name',
        type=str,
        default='labels_softmax:0',
        help='Name of node outputting a prediction in the model.')


    parser.add_argument('--samples_per_class',
                        type=int,
                        default=13000,
                        help='Number of samples to draw per class')
    parser.add_argument(
        '--background_volume',
        type=float,
        default=settings.background_volume,
        help="""\
        How loud the background noise should be, between 0 and 1.
        """)
    parser.add_argument(
        '--background_frequency',
        type=float,
        default=settings.background_frequency,
        help="""\
        How many of the training samples have background noise mixed in.
        """)
    parser.add_argument(
        '--silence_percentage',
        type=float,
        default=settings.silence_percentage,
        help="""\
        How much of the training data should be silence.
        """)
    parser.add_argument(
        '--unknown_percentage',
        type=float,
        default=settings.unknown_percentage,
        help="""\
        How much of the training data should be unknown words.
        """)
    parser.add_argument(
        '--time_shift_ms',
        type=float,
        default=settings.time_shift_ms,
        help="""\
        Range to randomly shift the training audio by in time.
        """)
    parser.add_argument(
        '--testing_percentage',
        type=int,
        default=settings.testing_percentage,
        help='What percentage of wavs to use as a test set.')
    parser.add_argument(
        '--validation_percentage',
        type=int,
        default=settings.validation_percentage,
        help='What percentage of wavs to use as a validation set.')
    parser.add_argument(
        '--sample_rate',
        type=int,
        default=settings.sample_rate,
        help='Expected sample rate of the wavs', )
    parser.add_argument(
        '--clip_duration_ms',
        type=int,
        default=settings.clip_duration_ms,
        help='Expected duration in milliseconds of the wavs', )
    parser.add_argument(
        '--clip_stride_ms',
        type=int,
        default=30,
        help='How often to run recognition. Useful for models with cache.', )
    parser.add_argument(
        '--window_size_ms',
        type=float,
        default=settings.window_size_ms,
        help='How long each spectrogram timeslice is', )
    parser.add_argument(
        '--window_stride_ms',
        type=float,
        default=settings.window_stride_ms,
        help='How long the stride is between spectrogram timeslices', )
    parser.add_argument(
        '--dct_coefficient_count',
        type=int,
        default=settings.dct_coefficient_count,
        help='How many bins to use for the MFCC fingerprint', )

    FLAGS, unparsed = parser.parse_known_args()

    sess = tf.InteractiveSession()
    create_inference_graph(FLAGS.wanted_words, FLAGS.sample_rate,
                           FLAGS.clip_duration_ms, FLAGS.clip_stride_ms,
                           FLAGS.window_size_ms, FLAGS.window_stride_ms,
                           FLAGS.dct_coefficient_count, FLAGS.model_architecture)
    models.load_variables_from_checkpoint(sess, FLAGS.start_checkpoint)

    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
